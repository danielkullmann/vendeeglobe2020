# Scrape Vendee Globe 2020 Statistics

This project contains a Scrapy spider to get race data from https://vendeglobe.org.

The goal is to create special kinds of graphs, e.g. for these questions:

* How do certain sailors progress in the rankings?

The webapp is automatically deployed on https://danielkullmann.gitlab.io/vendeeglobe2020/.

## How to Use the Spider

First, you need to have scrapy installed:

```bash
pip install scrapy
```

Then run the script `crawl.py`, which does all the necessary things.

## TODOs

Rather than having scrapy append to the csv file, it should write its data into
a new file, and then integrate the contents of existing and new file.  The
reason for this is that I don't see a guarantee from Scrapy that the order of
columns will always be the same.
