import scrapy

from functools import partial
from urllib.parse import urlparse
import zipfile
from io import BytesIO
import xml.etree.ElementTree as ET
import re

domain = 'www.vendeeglobe.org'

SCHEMA = "{http://schemas.openxmlformats.org/spreadsheetml/2006/main}"

"""28°50.30'N 26°24.56'W"""
LAT_LONG_RE = re.compile("^([0-9]+)°([0-9.]+)'([NWSE])$") 

def removeSuffix(suffix, string):
    if suffix and string.endswith(suffix):
        return string[:-len(suffix)]
    else:
        return string

def removeSuffixAndParseFloat(suffix, string):
    if suffix and string.endswith(suffix):
        return float(string[:-len(suffix)])
    else:
        return float(string)

def convertLatLong(input):
    m = LAT_LONG_RE.match(input)
    degrees = float(m.group(1))
    minutes = float(m.group(2))
    sign = +1 if m.group(3) in ["N", "W"] else -1
    return sign * degrees + minutes/60

fields = {
    "timestamp": {
    },
    "row": {
        "name": "Rank",
        "converter": int
    },
    "B": {
        "name": "Official Rank",
        "converter": int
    },
    "C": {
        "skip": True
    },
    "D": {
        "name": "Name"
    },
    "E": {
        "skip": True
    },
    "F": {
        "name": "Latitude",
        "converter": convertLatLong
    },
    "G": {
        "name": "Longitude",
        "converter": convertLatLong
    },
    "H": {
        "name": "Heading (°) in the last 30 minutes",
        "converter": partial(removeSuffixAndParseFloat, "°")
    },
    "I": {
        "name": "Speed (knots) in the last 30 minutes",
        "converter": partial(removeSuffixAndParseFloat, " kts")
    },
    "J": {
        "name": "VMG (knots) in the last 30 minutes",
        "converter": partial(removeSuffixAndParseFloat, " kts")
    },
    "K": {
        "name": "Distance (nm) in the last 30 minutes",
        "converter": partial(removeSuffixAndParseFloat, " nm")
    },
    "L": {
        "name": "Heading (°) since the last report",
        "converter": partial(removeSuffixAndParseFloat, "°")
    },
    "M": {
        "name": "Speed (knots) since the last report",
        "converter": partial(removeSuffixAndParseFloat, " kts")
    },
    "N": {
        "name": "VMG (knots) since the last report",
        "converter": partial(removeSuffixAndParseFloat, " kts")
    },
    "O": {
        "name": "Distance (nm) since the last report",
        "converter": partial(removeSuffixAndParseFloat, " nm"),
        "zero_first_value": True
    },
    "P": {
        "name": "Heading (°) in the last 24 hours",
        "converter": partial(removeSuffixAndParseFloat, "°")
    },
    "Q": {
        "name": "Speed (knots) in the last 24 hours",
        "converter": partial(removeSuffixAndParseFloat, " kts")
    },
    "R": {
        "name": "VMG (knots) in the last 24 hours",
        "converter": partial(removeSuffixAndParseFloat, " kts")
    },
    "S": {
        "name": "Distance (nm) in the last 24 hours",
        "converter": partial(removeSuffixAndParseFloat, " nm")
    },
    "T": {
        "name": "Distance to the finish",
        "converter": partial(removeSuffixAndParseFloat, " nm")
    },
    "U": {
        "name": "Distance to the leader",
        "converter": partial(removeSuffixAndParseFloat, " nm")
    }

}

def convert(values):
    result = dict()
    for key, value in values.items():
        if key in fields:
            definition = fields[key]
            name = definition.get("name", key)
            if "skip" in definition and definition["skip"]: continue
            if "converter" in definition:
                value = definition["converter"](value)
            result[name] = value
    return result

class VendeeGlobe2020Rankings(scrapy.Spider):
    name = 'rankings'
    allowed_domains = [domain]

    def __init__(self, existing_timestamps=None, *args, **kwargs):
        super(VendeeGlobe2020Rankings, self).__init__(*args, **kwargs)
        self.start_urls = ["https://" + domain + "/en/ranking/"]
        self.existing_timestamps = existing_timestamps
        if self.existing_timestamps is None:
            self.existing_timestamps = set()

    def parse(self, response):
        # select class="form__input m--select onsubmit-rank" data-uri="/en/ranking">
        for element in response.css("select.onsubmit-rank option"):
            timestamp = element.attrib["value"].strip()
            if timestamp == "" or timestamp in self.existing_timestamps: continue
            print("need to dowload data for " + timestamp)
            next_page = "https://" + domain + "/en/ranking/" + timestamp
            yield scrapy.Request(next_page, callback=partial(self.parse_ranking, timestamp))

    def parse_ranking(self, timestamp, response):
        # <div class="rankings__item">
        excel_path = response.css("a.rankings__download::attr(href)").get()
        if excel_path is not None:
            next_page = "https://" + domain + excel_path
            yield scrapy.Request(next_page, callback=partial(self.parse_excel, timestamp))

    def parse_excel(self, timestamp, response):
        fh = BytesIO()
        fh.write(response.body)
        zip = zipfile.ZipFile(fh)
        fh_sheet = zip.open("xl/worksheets/sheet1.xml")
        sheet = fh_sheet.read()
        fh_sheet.close()
        fh_shared_strings = zip.open("xl/sharedStrings.xml")
        shared_strings = fh_shared_strings.read()
        fh_shared_strings.close()
        zip.close()
        yield from self.parse_excel_content(sheet, shared_strings, timestamp)

    def parse_excel_content(self, sheets_xml, shared_strings_xml, timestamp):

        shared_strings_xml = ET.fromstring(shared_strings_xml)
        shared_strings = []
        for child in shared_strings_xml:
            content = ""
            t_values = []
            self.find_t_values(child, t_values)
            shared_strings.append(t_values[0])

        sheets_xml = ET.fromstring(sheets_xml)
        for row in sheets_xml.iter(SCHEMA+"row"):
            row_number = int(row.attrib["r"])
            if row_number < 6: continue
            values = dict()
            values["timestamp"] = timestamp
            values["row"] = row_number - 5 # Because i starts at 6
            for col in row.iter(SCHEMA+"c"):
                col_coord = col.attrib["r"][0]
                text = ""
                for value in col.iter(SCHEMA+"v"):
                    shared_index = int(value.text)
                    text += shared_strings[shared_index]
                if text != "":
                    values[col_coord] = text
            if len(values) == 22:
                yield convert(values)


    def find_t_values(self, element, results):
        if element.tag == SCHEMA+"t":
            t = element.text
            if t and t.strip() != "":
                results.append(t.strip())
        for child in element:
            self.find_t_values(child, results)
