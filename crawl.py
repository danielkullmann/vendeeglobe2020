#!/usr/bin/env python3

import csv
from io import BytesIO
import os
from scrapy.crawler import CrawlerProcess
from vendeeglobe2020.spiders.rankings import VendeeGlobe2020Rankings, fields

filename = "vendeeglobe2020.csv"

# Determine existing data, so we can skip data that we have already downloaded
existing_timestamps = set()
if os.path.exists(filename):
    with open(filename) as fh:
        reader = csv.reader(fh)
        try:
            header = next(reader)
            rows = list(reader)
            timestamp_column = header.index("timestamp")
            existing_timestamps = { row[timestamp_column] for row in rows }
        except StopIteration:
            pass

process = CrawlerProcess(settings={
    "CONCURRENT_REQUESTS": 2,
    "TELNETCONSOLE_ENABLED": False,
    "LOG_LEVEL": "WARN",
    "FEEDS": {
        filename: {"format": "csv"},
    }
})
process.crawl(VendeeGlobe2020Rankings, existing_timestamps=existing_timestamps)

process.start()

# Postprocess Data: 
# * some fields must be set to zero
# * here, data can be aggregated, like "Distance (nm) since last report"
zero_first_value_fields = []
aggregated = "Distance (nm) since the start"
aggregate = "Distance (nm) since the last report"
aggregated_values = dict() # name of skipper -> aggregated values
for key, field in fields.items():
    if "zero_first_value" in field and field["zero_first_value"]:
        zero_first_value_fields.append(field["name"] if "name" in field else key)

with open(filename) as fh:
    reader = csv.reader(fh)
    header = reader.__next__()

    name_column = header.index("Name")
    if aggregated not in header:
        header.append(aggregated)
    aggregate_column = header.index(aggregate)
    aggregated_column = header.index(aggregated)
    rows = list(reader)

    # TODO are we sure header coluns will always be in the same order?
    rows = [row for row in rows if row[0] != header[0]]

    timestamp_column = header.index("timestamp")
    timestamps = list({ row[timestamp_column] for row in rows })
    timestamps.sort()
    first_timestamp = timestamps[0]

    # I need to sort the rows by timestamp so that values can be aggregated
    rows.sort(key = lambda e: e[timestamp_column])

    for row in rows:
        # make values zero where necessary
        timestamp = row[timestamp_column]
        if timestamp == first_timestamp:
            for zero_first_value_field in zero_first_value_fields:
                row[header.index(zero_first_value_field)] = "0.0"

        # aggregate value
        skipper_name = row[name_column]
        if skipper_name not in aggregated_values:
            aggregated_values[skipper_name] = 0.0
        aggregated_values[skipper_name] += float(row[aggregate_column])
        if len(row) <= aggregated_column:
            row.append(0.0)
        row[aggregated_column] = aggregated_values[skipper_name]

with open(filename, "w") as fh:
    writer = csv.writer(fh)
    writer.writerow(header)
    writer.writerows(rows)
