"use strict";

// raw csv data loaded; two fields header and lines
var csv = {};
// what is selected by default in the dropdown?
var defaultDataColumnName = "Rank";
// currently selected item in dropbox
var selectedDataColumnName = null;
var aAllSkippers = [];
// list of skipper names, sorted by current ranking
var aSkippersRanking = [];
// mapping from skipper name to his rank
var mSkipperToRank = {};
// mapping from skipper name to his datasetIndex in the chart
var mSkipperToDatasetIndex = {};
// How many timestamps should be shown in the graph
// always the latest n timestamps
// <=0 means all
var iShowTimeStamps = null;

var oChart = null;

var mPlotOptions = {
    maintainAspectRatio: false,
    legend: {
        display: false
    },
    plugins:  {
        zoom: {
            zoom: {
                enabled: true,
                drag: true,
                mode: "x"
            }
        },
        colorschemes: {
            scheme: 'brewer.SetOne9'
        }
    }
};

function toggleDelta() {
    reloadChart();
}

/*
  sortAscending: Whether showing the top n means showing the
    n smallest (true) or n largest (false) values
 */
var mKeySettings = {
    "Rank": {
        sortAscending: true
    },
    "Official Rank": {
        sortAscending: true
    },
    "Name": {
        sortAscending: true
    },
    "Latitude": {
        sortAscending: true
    },
    "Longitude": {
        sortAscending: true
    },
    "Heading": {
        sortAscending: true
    },
    "Speed": {
        sortAscending: false
    },
    "VMG": {
        sortAscending: false
    },
    "Distance": {
        sortAscending: false
    },
    "Distance to the finish": {
        sortAscending: true
    },
    "Distance to the leader": {
        sortAscending: true
    }
};

function getKeySetting(sKeyName) {
    var aKeys = Object.keys(mKeySettings);
    var sFoundKey = null;
    for (var i = 0; i < aKeys.length; i++) {
        var sKey = aKeys[i];
        if (sKeyName.startsWith(sKey)) {
            if (sFoundKey === null || sFoundKey.length < sKey.length) {
                sFoundKey = sKey;
            }
        }
    }
    return mKeySettings[sFoundKey];
}

function showTop(num) {
    var mRanking = determineCurrentRankingFor(selectedDataColumnName);
    for (var i=0; i < aAllSkippers.length; i++) {
        var name = aAllSkippers[i];
        var rank = mRanking[name] || aAllSkippers.length;
        var datasetIndex = mSkipperToDatasetIndex[name];
        setVisibilityForDataset(datasetIndex, rank<=num, true);
    }
    reloadChart();
}

function showTimestamps(num) {
  iShowTimeStamps = num;
  reloadChart();
}

function createChart(chartData) {
    var ctx = document.getElementById("plot1");
    if (oChart && oChart.destroy) {
        oChart.destroy();
    }

    oChart = new Chart(ctx, {
        type: 'line',
        data: chartData,
        options: mPlotOptions
    });

    ctx.addEventListener("dblclick", function() {
        oChart.resetZoom();
    });

    window.setTimeout(createCustomLegend, 0);
}

function setVisibilityForDataset(datasetIndex, visible, dontReloadChart) {
    var oLegend = document.getElementById("legend");
    var oSkipper = oLegend.getElementsByClassName("legend-"+datasetIndex)[0];
    var oLabel = oSkipper.getElementsByClassName("label")[0];
    var meta = oChart.getDatasetMeta(datasetIndex);
    if (visible) {
        oLabel.style = "text-decoration: underline; font-weight: bold;";
        meta.hidden = false;
    } else {
        oLabel.style = "text-decoration: none; font-weight: lighter;";
        meta.hidden = true;
    }
    if (!dontReloadChart) {
        reloadChart();
    }
}

function toggleVisibilityForDataset(datasetIndex) {
    setVisibilityForDataset(datasetIndex, !oChart.isDatasetVisible(datasetIndex));
}

function toggleVisibilityForLegend(event) {
    var el = event.target;
    while (el.name === null || el.name === undefined) {
        el = el.parentElement;
    }
    var datasetIndex = parseInt(el.name, 10);
    toggleVisibilityForDataset(datasetIndex);
 }

function createCustomLegend() {
    var oLegend = document.getElementById("legend");
    if (oLegend.children.length > 0) return;
    var schemePath = mPlotOptions.plugins.colorschemes.scheme.split(".");
    var oScheme = Chart.colorschemes;
    for (var el of schemePath) {
        oScheme = oScheme[el];
    }
    var oTable = document.createElement("table");
    oTable.style = "font-size: smaller;";
    var oTr = document.createElement("tr");
    for (var i=0; i < oChart.data.datasets.length; i++) {
        var oDataset = oChart.data.datasets[i];
        var oTd = document.createElement("td");
        oTd.className = "legend-" + i;
        oTd.name = i;
        var oColorBox = document.createElement("span");
        oColorBox.style = "display: inline-block; vertical-align: middle; margin: 5px 0px; height: 20px; width: 20px; background-color: " + oScheme[i % oScheme.length];
        var oLabel = document.createElement("span");
        oLabel.className = "label";
        var rank = mSkipperToRank[oDataset.label] || "NL";
        oLabel.append(oDataset.label + " (" + rank + ")");
        if (oChart.isDatasetVisible(i)) {
            oLabel.style = "text-decoration: underline; font-weight: bold;";
        } else {
            oLabel.style = "text-decoration: none; font-weight: normal;";
        }
        oTd.append(oColorBox);
        oTd.append(" ");
        oTd.append(oLabel);
        oTd.onclick = toggleVisibilityForLegend;
        oTr.append(oTd);
        if (i % 5 == 4 ) {
            oTable.append(oTr);
            oTr = document.createElement("tr");
        }
        oTable.append(oTr);
        oLegend.append(oTable);
    }
}

function parseFloatWhenPossible(value) {
    var regex = new RegExp("^[0-9]+(\\.[0-9]*)?$");
    if (regex.test(value)) {
        var f = parseFloat(value);
        if (isNaN(f)) {
            return value;
        } else {
            return f;
        }
    } else {
        return value;
    }
}

function parseCSV(str) {
    var lines = str.split("\r\n");
    var header = null;
    var content = [];
    for (var i = 0; i < lines.length; i++) {
        var line = lines[i];
        if (line === "") continue;
        var columns = line.split(",");
        if (header === null) {
            header = columns;
        } else {
            content.push(columns.map(parseFloatWhenPossible));
        }
    }
    return {header: header, lines: content};
}

function getLatestTimestampData() {
    var header = csv.header;
    var lines = csv.lines;
    var timestampColumn = header.indexOf("timestamp");
    var timestamps = Array.from(new Set(lines.map(function(item) { return item[timestampColumn]; } )));
    timestamps.sort();
    var latestTimestamp = timestamps[timestamps.length-1];
    var latestTimestampData = lines.filter(function(item) { return item[timestampColumn] == latestTimestamp; });
    return latestTimestampData;
}

function compareByKey(keyColumn, i1, i2) {
    var rank1 = i1[keyColumn];
    var rank2 = i2[keyColumn];
    if (rank1 < rank2) {
        return -1;
    }
    if (rank1 > rank2) {
        return 1;
    }
    return 0;
}

function determineCurrentRankingFor(key) {
    var mRanking = {};
    var bSortAscending = getKeySetting(key).sortAscending;
    var header = csv.header;
    var lines = csv.lines;
    var nameColumn = header.indexOf("Name");
    var keyColumn = header.indexOf(key);

    var latestTimestampData = getLatestTimestampData();
    latestTimestampData.sort(compareByKey.bind(null, keyColumn));
    if (!bSortAscending) {
        latestTimestampData.reverse();
    }

    var aReverseRanking = latestTimestampData.map(item => item[nameColumn]);
    for (var i = 0; i < aReverseRanking.length; i++) {
        mRanking[aReverseRanking[i]] = i+1;
    }
    return mRanking;
}

function determineCurrentRanking() {
    aSkippersRanking = {};

    var header = csv.header;
    var lines = csv.lines;
    var timestampColumn = header.indexOf("timestamp");
    var nameColumn = header.indexOf("Name");
    var rankColumn = header.indexOf("Rank");

    var latestTimestampData = getLatestTimestampData();
    latestTimestampData.sort(compareByKey.bind(null, rankColumn));

    aAllSkippers = Array.from(new Set(lines.map(function(item) { return item[nameColumn]; } )));
    aSkippersRanking = latestTimestampData.map(item => item[nameColumn]);
    mSkipperToRank = new Map();
    for (var i = 0; i < aSkippersRanking.length; i++) {
        mSkipperToRank[aSkippersRanking[i]] = i+1;
    }
}

function parseTimestamp(timestamp) {
    var year = timestamp.slice(0,4);
    var month = timestamp.slice(4,6);
    var day = timestamp.slice(6,8);
    var hour = timestamp.slice(9,11);
    var minute = timestamp.slice(11,13);
    return year + "-" + month + "-" + day + " " + hour + ":" + minute;
}

/** Converts the data from the source csv into a meaningful structure.
 *
 * The input data has columns timestamp, rank, official_rank, name,
 * and all the columns containing the actual data.
 *
 * @param {array} csv
 * @returns {object} object suitable for giving as data to Chart.js
 */
function createChartData() {
    var i;
    var toShow;
    var storedToShow = window.localStorage.getItem("to-show");
    var storedDataColumnName = window.localStorage.getItem("data-column-name");
    var storedLastTimestamps = window.localStorage.getItem("last-timestamps");
    if (oChart && oChart.data && oChart.data.datasets) {
        toShow = new Set();
        for (i = 0; i < oChart.data.datasets.length; i++) {
            if (oChart.isDatasetVisible(i)) {
                toShow.add(oChart.data.datasets[i].label);
            }
        }
    } else {
        if (storedToShow) {
            toShow = new Set(JSON.parse(storedToShow));
        } else {
            toShow = new Set(aSkippersRanking.slice(0,5));
        }
    }
    if (selectedDataColumnName === null) {
        if (storedDataColumnName) {
            selectedDataColumnName = storedDataColumnName;
        } else {
            selectedDataColumnName = defaultDataColumnName;
        }
        // Create this again so that the correct item is selected
        fillDataColumnNameSelector();
    }
    if (iShowTimeStamps === null) {
      if (storedLastTimestamps) {
        iShowTimeStamps = storedLastTimestamps;
      } else {
        iShowTimeStamps = 0;
      }
      // Create this again so that the correct item is selected
      fillDataColumnNameSelector();
    }
    window.localStorage.setItem("to-show", JSON.stringify(Array.from(toShow)));
    window.localStorage.setItem("last-timestamps", iShowTimeStamps);
    window.localStorage.setItem("data-column-name", selectedDataColumnName);

    var showDelta = document.getElementById("show-delta").checked;

    var header = csv.header;
    var lines = csv.lines;
    var timestampColumn = header.indexOf("timestamp");
    var nameColumn = header.indexOf("Name");
    var dataColumn = header.indexOf(selectedDataColumnName);

    var timestamps = Array.from(new Set(lines.map(function(item) { return item[timestampColumn]; } )));
    timestamps.sort();
    var latest = timestamps[timestamps.length-1];
    document.getElementById("latest-timestamp").innerHTML = parseTimestamp(latest);

    if (iShowTimeStamps > 0 && timestamps.length > iShowTimeStamps) {
      timestamps = timestamps.slice(timestamps.length - iShowTimeStamps);
    }

    var names = Array.from(new Set(lines.map(function(item) { return item[nameColumn]; } )));
    names.sort();

    var fnFilterNames = function(item) { return item[nameColumn] == name; };
    var fnFilterTimestamp = function(item) { return item[timestampColumn] == timestamp; };

    var datasets = [];
    for (i=0; i < names.length; i++) {
        var name = names[i];
        mSkipperToDatasetIndex[name] = i;
        var nameData = lines.filter(fnFilterNames);
        var dataset = {
            label: name,
            data: [],
            fill: false,
            borderWidth: 3,
            hoverBorderWidth: 6,
            hidden: !toShow.has(name)
        };
        var lastValue = null;
        for (var timestamp of timestamps) {
            var tsLine = nameData.filter(fnFilterTimestamp)[0];
            if (tsLine) {
                var value = tsLine[dataColumn];
                if (showDelta) {
                    if (lastValue === null) {
                        lastValue = value;
                        value = 0.0;
                    } else {
                        var temp = value;
                        value = value - lastValue;
                        lastValue = temp;
                    }
                }
                dataset.data.push(value);
            } else {
                dataset.data.push(null);
            }
        }
        datasets.push(dataset);
    }

    var timestamp_labels = timestamps.map(parseTimestamp);

    var result = {
        labels: timestamp_labels,
        datasets: datasets
    };

    return result;
}

function selectDataColumnName(event) {
    selectedDataColumnName = event.target.value;
    reloadChart();
}

function fillDataColumnNameSelector() {
    var oSelect = document.getElementById("select-data-column-name");
    oSelect.onchange = selectDataColumnName;

    // Remove existing options
    while (oSelect.options.length) {
        oSelect.remove(0);
    }

    var header = csv.header;
    // All columns are data columns, except "timestamp" and "Name"
    var dataColumns = header.filter(name => name != "timestamp" && name != "Name");
    dataColumns.sort();

    for (var i = 0; i < dataColumns.length; i++) {
        var dataColumnName = dataColumns[i];
        var oElement = document.createElement("option");
        oElement.value = dataColumnName;
        oElement.append(dataColumnName.replaceAll("_", " "));
        oSelect.append(oElement);
    }

    oSelect.value = selectedDataColumnName;
}

function reloadChart() {
    var chartData = createChartData();
    createChart(chartData);
}

function loadData() {
    fetch("./vendeeglobe2020.csv?t="+new Date().getTime()).then(oResponse => {
        oResponse.text().then(oResponse => {
            csv = parseCSV(oResponse);
            fillDataColumnNameSelector();
            determineCurrentRanking();
            reloadChart();
        });
    });
}

window.onload = function() {
    document.getElementById("show-delta").checked = false;
    document.getElementById("show-delta").addEventListener("change", toggleDelta);
    loadData();
};
